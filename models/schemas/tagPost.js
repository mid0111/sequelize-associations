'use strict';

module.exports = function(sequelize, DataTypes) {
  var TagPost = sequelize.define('tagPost', {
    tagId: DataTypes.INTEGER,
    postId: DataTypes.INTEGER
  }, {
    tableName: 'tag-post',
    freezeTableName: true
  });

  return TagPost;
};
