'use strict';

module.exports = function(sequelize, DataTypes) {
  var Post = sequelize.define('post', {
    title: DataTypes.STRING
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {
        Post.belongsTo(models.user, {
          foreignKey: {
            field: 'userId',
            name: 'userId',
            allowNull: false
          }
        });

        Post.belongsToMany(models.tag, {
          through: {
            model: models.tagPost,
            unique: false
          },
          foreignKey: 'postId'
        });
      }
    }
  });

  return Post;
};
