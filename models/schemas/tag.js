'use strict';

module.exports = function(sequelize, DataTypes) {
  var Tag = sequelize.define('tag', {
    name: DataTypes.STRING
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {
        Tag.belongsToMany(models.post, {
          through: {
            model: models.tagPost,
            unique: false
          },
          foreignKey: 'tagId'
        });
      }
    }
  });

  return Tag;
};
