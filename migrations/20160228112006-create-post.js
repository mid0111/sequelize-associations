'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface
      .createTable('post', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
        userId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'user',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        title: Sequelize.STRING
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface
      .dropTable('post');
  }
};
