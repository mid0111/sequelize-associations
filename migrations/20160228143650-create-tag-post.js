'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface
      .createTable('tag-post', {
        tagId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'tag',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        postId: {
          type: Sequelize.INTEGER,
          references: {
            model: 'post',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
      });
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface
      .dropTable('tag-post');
  }
};
