sequelize-associations
====

Sequelize の associations と仲良くするプロジェクト.

## モデルの定義

### user - post (1 : n)

* ユーザを持たないポストは許可しない
  
### post - tag (0..n : 0..n)

* タグを持たないポストを許可
* ポストを持たないタグを許可