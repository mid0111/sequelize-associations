'use strict';

var expect = require('chai').expect;
var Promise = require('bluebird');
var User = require('../models').user;
var Post = require('../models').post;

describe('Post モデル', function() {

  before(function() {
    return Promise.all[
      Post.destroy({ truncate: true }),
      User.destroy({ truncate: true })
    ];
  });

  describe('create', function() {
    it('ユーザに紐付かないPostの作成ができないこと', function () {
      return Post.create({
        title: 'Post for test.'
      }).then(function() {
        throw new Error();
      }, function(err) {
        expect(err).not.to.be.null;
      });
    });

    it('ユーザに紐付くPostの作成ができること', function () {
      return User.create({
        name: 'testUser'
      }).then(function(user) {
        return Post.create({
          title: 'Post for test.',
          userId: user.id
        }).then(function(post) {
          expect(post.id).to.be.a('number');
          expect(post.createdAt).to.be.a('date');
          expect(post.updatedAt).to.be.a('date');
          expect(post.title).to.equal('Post for test.');
          expect(post.userId).to.equal(user.id);
        });
      });
    });
  });


  describe('find', function() {

    var targetId;
    var userName = 'testUser001';
    var title = 'Post for search test.';

    before(function() {
      return User.create({
        name: userName
      }).then(function(user) {
        return Post.create({
          title: title,
          userId: user.id
        }).then(function(post) {
          targetId = post.id;
        });
      });
    });

    it('User情報を含めた結果を取得できること', function () {
      return Post.findById(targetId, {
        include: [User]
      }).then(function(post) {
        // Post
        expect(post.id).to.be.a('number');
        expect(post.createdAt).to.be.a('date');
        expect(post.updatedAt).to.be.a('date');
        expect(post.title).to.equal(title);

        // User
        expect(post.user.id).to.be.a('number');
        expect(post.user.name).to.equal(userName);
      });
    });

  });

});
