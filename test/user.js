'use strict';

var expect = require('chai').expect;
var Promise = require('bluebird');
var User = require('../models').user;
var Post = require('../models').post;

describe('User モデル', function() {

  before(function() {
    return Promise.all[
      Post.destroy({ truncate: true }),
      User.destroy({ truncate: true })
    ];
  });

  describe('create', function() {
    it('ユーザの作成ができること', function () {
      return User.create({
        name: 'testName'
      }).then(function(user) {
        expect(user.id).to.be.a('number');
        expect(user.createdAt).to.be.a('date');
        expect(user.updatedAt).to.be.a('date');
        expect(user.name).to.equal('testName');
      });
    });
  });

  describe('find', function() {
    var targetId;
    var userName = 'test user 002';
    var title = 'title for user test.';

    before(function() {
      return User.create({
        name: userName
      }).then(function(user) {
        targetId = user.id;
        return Post.create({
          title: title,
          userId: user.id
        });
      });
    });

    it('ユーザに紐づくPostの一覧ができること', function () {
      return User.findById(targetId, {
        include: [Post]
      }).then(function(user) {
        expect(user.id).to.be.a('number');
        expect(user.createdAt).to.be.a('date');
        expect(user.updatedAt).to.be.a('date');
        expect(user.name).to.equal(userName);

        // Post
        expect(user.posts).to.be.an('Array');
        expect(user.posts[0].title).to.equal(title);
      });
    });
  });

  describe('delete', function() {
    var userId;
    var postId;
    var userName = 'test user 002';
    var title = 'title for user test.';

    before(function() {
      return User.create({
        name: userName
      }).then(function(user) {
        userId = user.id;
        return Post.create({
          title: title,
          userId: user.id
        }).then(function(post) {
          postId = post.id;
        });
      });
    });

    it('ユーザ削除時に紐づくPostが同時に削除されること', function () {
      return User.destroy({
        where: {
          id: userId
        }
      }).then(function(rows) {
        expect(rows).to.equal(1);

        Post.findById(postId).then(function(post) {
          expect(post).to.be.null;
        });
      });
    });
  });

});
