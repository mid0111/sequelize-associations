'use strict';

var expect = require('chai').expect;
var Promise = require('bluebird');
var Tag = require('../models').tag;
var User = require('../models').user;
var Post = require('../models').post;

describe('Tag モデル', function() {

  describe('create', function() {

    it('Post に紐付かない Tag の作成ができること', function () {
      return Tag.create({
        name: 'testTag'
      }).then(function(tag) {
        expect(tag.id).to.be.a('number');
        expect(tag.createdAt).to.be.a('date');
        expect(tag.updatedAt).to.be.a('date');
        expect(tag.name).to.equal('testTag');
      });
    });

    it('Post に紐付けて Tag の作成ができること', function () {
      return User.create({
        name: 'test user'
      }).then(function(user) {
        return Tag.create({
          name: 'てすとっす',
          posts: [{
            title: 'testたいとるなんです',
            userId: user.id
          }]
        }, {
          include: [Post]
        }).then(function(tag) {
          expect(tag.id).to.be.a('number');
          expect(tag.createdAt).to.be.a('date');
          expect(tag.updatedAt).to.be.a('date');
          expect(tag.name).to.equal('てすとっす');

          expect(tag.posts).to.be.an('Array');
          expect(tag.posts[0].title).to.equal('testたいとるなんです');
        });
      });
    });
  });

  describe('find', function() {
    var targetId;
    var tagName = 'たぐっす';
    var postTitle = 'ぽすとのたいとる';

    before(function() {
      return User.create({
        name: 'test user'
      }).then(function(user) {
        return Tag.create({
          name: tagName,
          posts: [{
            title: postTitle,
            userId: user.id
          }]
        }, {
          include: [Post]
        }).then(function(tag) {
          targetId = tag.id;
        });
      });
    });

    it('Post 情報を含めてタグの一覧ができること', function () {
      return Tag.findById(targetId, {
        include: [Post]
      }).then(function(tag) {
        expect(tag.id).to.be.a('number');
        expect(tag.createdAt).to.be.a('date');
        expect(tag.updatedAt).to.be.a('date');
        expect(tag.name).to.equal(tagName);

        // Post
        expect(tag.posts).to.be.an('Array');
        expect(tag.posts[0].title).to.equal(postTitle);
      });
    });
  });

  describe('delete', function() {
    var tagId;
    var postId;
    var tagName = 'たぐっす';
    var postTitle = 'ぽすとのたいとる';

    before(function() {
      return User.create({
        name: 'test user'
      }).then(function(user) {
        return Tag.create({
          name: tagName,
          posts: [{
            title: postTitle,
            userId: user.id
          }]
        }, {
          include: [Post]
        }).then(function(tag) {
          tagId = tag.id;
          postId = tag.posts[0].id;
        });
      });
    });

    it('タグ削除時にPostとの関連が同時に削除されること', function () {
      return Post.findById(postId, {include: [Tag]}).then(function(post) {
        // 削除前確認
        expect(post.tags).to.have.length(1);
      }).then(function() {
        return Tag.destroy({
          where: {
            id: tagId
          }
        }).then(function(rows) {
          expect(rows).to.equal(1);

          return Post.findById(postId, {include: [Tag]}).then(function(post) {
            // 削除確認
            expect(post.tags).to.have.length(0);
          });
        });
      });
    });
  });

});
